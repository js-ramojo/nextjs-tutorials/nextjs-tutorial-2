import MeetupList from '../components/meetups/MeetupList';

const DUMMY_MEETUPS = [
  {
  id: 'm1',
  title: 'First Meetup',
  image: 'https://external-preview.redd.it/y9I_5VX10Igpt5VEvzBAow4fkbV2d1JpdL5qmL4pZtM.jpg?auto=webp&s=cedbf018003ffd6b2d8898ef7f448d9c803b8136',
  address: 'Some address 3,12345,Nbi',
  Description: 'This is the first meetup'
  },
  {
    id: 'm2',
    title: 'Second Meetup',
    image: 'https://external-preview.redd.it/y9I_5VX10Igpt5VEvzBAow4fkbV2d1JpdL5qmL4pZtM.jpg?auto=webp&s=cedbf018003ffd6b2d8898ef7f448d9c803b8136',
    address: 'Some address 3,12345,Nbi',
    Description: 'This is the second meetup'
  
    }
]

function HomePage(){

  return <MeetupList meetups={DUMMY_MEETUPS} />

  
}

export default HomePage;